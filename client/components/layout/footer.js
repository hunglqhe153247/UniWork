import styles from '../../public/static/css/Footer.module.scss';
import classNames from 'classnames/bind';
import Image from 'next/image';
import FPTImage from '../../public/static/images/FptUni.png';
const cx = classNames.bind(styles);
function Footer() {
	return (
		<footer className={cx('wrapper')}>
			<div className={cx('image')}>
				{/* <img className={cx('footerImg')} src={'FPTImage'} alt='something'></img> */}
				<Image
					src='/static/images/FptUni.png'
					width='120'
					height='120'
					alt='img'></Image>
			</div>
			<div className={cx('image')}>
				<Image
					src='/static/images/FptUni.png'
					width='120'
					height='120'
					alt='img'></Image>
			</div>
			<div className={cx('image')}>
				<Image
					src='/static/images/FptUni.png'
					width='120'
					height='120'
					alt='img'></Image>
			</div>
			<div className={cx('image')}>
				<Image
					src='/static/images/FptUni.png'
					width='120'
					height='120'
					alt='img'></Image>
			</div>
		</footer>
	);
}

export default Footer;
