import styles from '../../public/static/css/Header.module.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);
function Header() {
	return (
		<header className={cx('wrapper')}>
			<div className={cx('inner')}>
				<div className={cx('logo')}>
					<a>Logo</a>
				</div>
				<div className={cx('action')}>
					<a href='/'>Trang chủ</a>
					<a href='/'>Dành cho nhà tuyển dụng</a>
					<a href='/'>Dành cho ứng viên</a>
					<a href='/'>Đăng nhập</a>
					<button>Đăng ký cho tài khoản</button>
				</div>
			</div>
		</header>
	);
}

export default Header;
