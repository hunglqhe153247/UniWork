require('isomorphic-fetch');
import styles from '../public/static/css/Home.module.scss';
import classNames from 'classnames/bind';
import Image from 'next/image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
import React from 'react';

const cx = classNames.bind(styles);
function MyPage() {
	return (
		<div className={cx('wrapper')}>
			<div className={cx('content')}>
				<div className={cx('intro')}>
					<h1>
						Tìm công việc{' '}
						<span>
							{' '}
							an toàn,
							<br /> phù hợp{' '}
						</span>{' '}
						cho sinh viên
					</h1>
					<p>
						Tìm công việc mới ngay với website Uniwork đơn giản
						<br />
						và dễ sử dụng cho người mới bắt đầu. Tìm việc
						<br />
						an toàn cho sinh viên chưa bao giờ dễ đến vậy với
						<br />
						Uniwork
					</p>
					<h4>Các từ khóa tìm việc trending:</h4>
				</div>
				<div className={cx('trending')}>
					<p>Pha chế</p>
					<p>Shipper</p>
					<p>Mẫu ảnh</p>
				</div>

				<div className={cx('searchContent')}>
					<FontAwesomeIcon
						className={cx('searchIcon')}
						icon={faMagnifyingGlass}></FontAwesomeIcon>
					<input placeholder='Tìm việc hoặc từ khóa'></input>
					<button className={cx('searchBtn')}>Search</button>
				</div>
			</div>
			<div className={cx('image')}>
				<Image
					width='200'
					height='200'
					src='/static/images/recruit.png'></Image>
			</div>
		</div>
	);
}

export default MyPage;
